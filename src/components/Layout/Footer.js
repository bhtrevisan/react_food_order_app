import React from "react";
import classes from "./Footer.module.css";

const Footer = () => {
  const year = new Date().getFullYear();

  return (
    <footer className={classes.footer}>
      <div>
        {`© ${year} Bruno Trevisan`}
      </div>
      <div>
        Contact me at <a href={'https://www.linkedin.com/in/bhtrevisan'}>Linkedin</a>
      </div>
      <div>
        Access to my <a href="https://gitlab.com/bhtrevisan">Gitlab</a>
      </div>
    </footer>
  );
};

export default Footer;