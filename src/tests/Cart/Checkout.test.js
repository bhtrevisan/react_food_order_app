import { render, screen } from "@testing-library/react";
import '@testing-library/jest-dom'
import Checkout from "../../components/Cart/Checkout";
import userEvent from "@testing-library/user-event";

describe('Cart components', () => {
  test('Render Checkout component', () => {
    render(<Checkout />);
    const nameLabel = screen.getByText("Your Name");
    expect(nameLabel).toBeInTheDocument();

    const streetLabel = screen.getByText("Street");
    expect(streetLabel).toBeInTheDocument();

    const postalLabel = screen.getByText("Postal Code");
    expect(postalLabel).toBeInTheDocument();

    const cityLabel = screen.getByText("City");
    expect(cityLabel).toBeInTheDocument();
  })

  test('Render buttons', () => {
    render(<Checkout />);
    const confirmButton = screen.getByTestId("confirmButton");
    expect(confirmButton).toBeInTheDocument();

    const cancelButton = screen.getByTestId("cancelButton");
    expect(cancelButton).toBeInTheDocument();
  })

  // Dont know why it is not working
  // test('Render invalid errors when Confirm is clicked and inputs are empty', () => {
  //   render(<Checkout />);
  //   const confirmButton = screen.getByTestId("confirmButton");
  //   const invalidName = screen.getByText("is Empty", {exact: false});
    
  //   userEvent.click(confirmButton);
  //   expect(invalidName).toBeInTheDocument();
  // })

})
