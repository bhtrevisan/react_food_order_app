import { render, screen } from "@testing-library/react";
import '@testing-library/jest-dom'
import App from "../App";


test('Render App component', () => { 
    render(<App />);
    const titleTest = screen.getByText(/Delicious Food/i);
    expect(titleTest).toBeInTheDocument();
 })

