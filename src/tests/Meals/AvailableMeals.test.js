import { render, screen } from "@testing-library/react";
import '@testing-library/jest-dom'
import userEvent from "@testing-library/user-event";
import AvailableMeals from "../../components/Meals/AvailableMeals";

describe('AvailableMeals component', () => {
  test('Render Checkout component', async () => {
    render(<AvailableMeals />);

    const listItems = await screen.findAllByRole('listitem', {}, {});
    expect(listItems).not.toHaveLength(0);

  })
})
