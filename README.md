## Food Order App

## 📜 Description
This project originated from a React course, serving as a practical exercise to reinforce skills in React Hooks and styling. 
The primary goal is to simulate a food ordering application, complete with essential features such as adding items to the cart, removing items from the cart, and completing the order process.
The project is integrated with a Firebase database to efficiently store and manage orders.

## 🖼️ Screenshots
I'll add some prints to show how this project looks.

### Main Screen
![MainScreen](./public/MainScreen.png)

### Cart Screen
![CartScreen](./public/CartScreen.png)

### Finish Order Screen
![FinishOrderScreen](./public/FinishOrderScreen.png)

## 💻 Requirements
You will need:
* npm;
* react;

## 🔨 Installation 
To install this project in your local machine, follow this steps:

Create a folder in your local machine.
Then run

`git clone {this project link}`

Go to project folder.

`npm install`


## ☕ Using Project
To use this project, you need to start this application on your localhost.

Go to project folder, which has te package.json and run:

`npm start`

## 🔧 Fixes and new features
This project is under development and the next steps are:
- Login option;
- Save your old orders and give an option to show it;
- Fix order screen size;
- ...

## 🙂 Author
[@bhtrevisan](https://gitlab.com/bhtrevisan)
